use std::{collections::HashSet, io::BufRead};

#[derive(Debug, Copy, Clone)]
enum Op {
    Nop(i32),
    Acc(i32),
    Jmp(i32),
}

type Program = Vec<Op>;

#[derive(Debug)]
enum State {
    Ok,
    Terminate,
    Error(String),
}
#[derive(Debug)]
struct Execution<'a> {
    program: &'a Program,
    accumulator: i32,
    program_counter: i32,
}

impl<'a> Execution<'a> {
    fn new(program: &'a Program) -> Self {
        Self {
            program,
            accumulator: 0,
            program_counter: 0,
        }
    }

    fn step(&mut self) -> State {
        if self.program_counter == self.program.len() as i32 {
            return State::Terminate;
        }

        let op = &self.program.get(self.program_counter as usize);
        if op.is_none() {
            return State::Error("program counter outside program range".to_owned());
        }
        let op = op.unwrap();

        match op {
            Op::Acc(v) => self.accumulator += v,
            Op::Jmp(v) => self.program_counter += v - 1, // v - 1, because PC is going to be incremented anyway
            Op::Nop(_) => (),
        }

        self.program_counter += 1;

        State::Ok
    }
}

fn read_input() -> Program {
    let mut result = Program::new();

    for line in std::io::stdin().lock().lines() {
        let line = line.unwrap();

        let parts: Vec<&str> = line.split_whitespace().collect();

        let op = match parts[0] {
            "nop" => Op::Nop(parts[1].parse().unwrap()),
            "acc" => Op::Acc(parts[1].parse().unwrap()),
            "jmp" => Op::Jmp(parts[1].parse().unwrap()),
            _ => panic!("unknown operation {}", parts[0]),
        };

        result.push(op);
    }

    result
}

fn execute_until_end(execution: &mut Execution) -> State {
    let mut visited: HashSet<i32> = HashSet::new();

    while !visited.contains(&execution.program_counter) {
        visited.insert(execution.program_counter);

        match execution.step() {
            State::Ok => continue,
            s @ State::Terminate => return s,
            s @ State::Error(_) => return s,
        }
    }

    State::Ok
}

fn part1(program: &Program) -> i32 {
    let mut execution = Execution::new(program);

    execute_until_end(&mut execution);

    execution.accumulator
}

fn part2(program: &Program) -> i32 {
    let mut temporary_program = program.to_owned();

    for (i, op) in program.iter().enumerate() {
        match op {
            Op::Nop(v) => temporary_program[i] = Op::Jmp(*v),
            Op::Jmp(v) => temporary_program[i] = Op::Nop(*v),
            _ => continue,
        }

        let mut execution = Execution::new(&temporary_program);

        let state = execute_until_end(&mut execution);
        println!("{}: {:?}", i, state);
        if let State::Terminate = state {
            return execution.accumulator;
        }

        temporary_program[i] = op.to_owned();
    }

    0
}

fn main() {
    let input = read_input();
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

use std::collections::{HashMap, HashSet};

use indoc::indoc;

type Tile = Vec<Vec<char>>;

fn print_tile(t: &Tile) {
    for line in t {
        for c in line {
            print!("{}", c);
        }
        println!();
    }
}

fn read_input() -> HashMap<i32, Tile> {
    let mut result = HashMap::new();

    loop {
        let mut buf = String::new();
        if let Ok(0) = std::io::stdin().read_line(&mut buf) {
            break;
        }

        let id: i32 = buf
            .trim()
            .strip_prefix("Tile ")
            .expect("line should start with Tile")
            .strip_suffix(":")
            .expect("line should end with :")
            .parse()
            .unwrap();

        buf.clear();
        while std::io::stdin().read_line(&mut buf).unwrap_or(0) > 1 {}

        result.insert(
            id,
            buf.trim()
                .split("\n")
                .map(|l| l.trim().chars().collect())
                .collect(),
        );
    }

    result
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Flip {
    None,
    Vertical,
    Horizontal,
}
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Orientation(Flip, i32);

fn parse_edges_flip(tile: &Tile, flip: Flip, values: &mut HashMap<Orientation, String>) {
    let size = tile.len() as i32;
    for rotation in 0..4 {
        // x, y, dx, dy
        // 0, 0, 1, 0
        // n, 0, 0, 1
        // n, n, -1, 0
        // 0, n, 0, -1

        let (mut x, mut y, dx, dy) = [
            (0, 0, 1, 0),
            (size - 1, 0, 0, 1),
            (size - 1, size - 1, -1, 0),
            (0, size - 1, 0, -1),
        ][rotation];

        let mut res = String::new();
        while x >= 0 && x < size && y >= 0 && y < size {
            res.push(tile[y as usize][x as usize]);
            x += dx;
            y += dy;
        }

        values.insert(Orientation(flip, rotation as i32), res);
    }
}

fn parse_edges(tile: &Tile) -> HashMap<Orientation, String> {
    let mut results = HashMap::new();
    parse_edges_flip(tile, Flip::None, &mut results);

    let vertical_flip: Tile = flip_tile(tile, &Flip::Vertical);
    parse_edges_flip(&vertical_flip, Flip::Vertical, &mut results);

    let horizontal_flip: Tile = flip_tile(tile, &Flip::Horizontal);
    parse_edges_flip(&horizontal_flip, Flip::Horizontal, &mut results);

    results
}

fn flip_tile(tile: &Tile, flip: &Flip) -> Tile {
    match flip {
        Flip::None => tile.to_owned(),
        Flip::Vertical => tile.iter().rev().cloned().collect(),
        Flip::Horizontal => tile
            .iter()
            .map(|l| l.iter().rev().cloned().collect())
            .collect(),
    }
}

fn rotate_tile(tile: &Tile) -> Tile {
    (0..tile.len())
        .map(|y| {
            (0..tile.len())
                .map(|x| tile[x][tile.len() - 1 - y])
                .collect()
        })
        .collect()
}

fn reconstruct_tile(tile: &Tile, orientation: &Orientation) -> Tile {
    let mut tile = flip_tile(tile, &orientation.0);

    for _ in 0..orientation.1 {
        tile = rotate_tile(&tile);
    }

    tile
}

struct Solver {
    tile_edges: HashMap<i32, HashMap<Orientation, String>>,
    edges: HashMap<String, Vec<(i32, Orientation)>>,
    width: usize,

    step: i32,
}

impl Solver {
    fn solve(&mut self, assignments: &Vec<(i32, Orientation)>) -> Option<Vec<(i32, Orientation)>> {
        self.step += 1;
        if self.step % 100000 == 0 {
            println!("{}", self.step);
        }
        if assignments.len() <= 2 {
            println!("{:?}", assignments);
        }
        if assignments.len() == self.tile_edges.len() {
            return Some(assignments.to_owned());
        }

        let mut assignments = assignments.to_owned();

        let top_constraint = if assignments.len() < self.width {
            None
        } else {
            assignments.get(assignments.len() - self.width)
        };
        let left_constraint = if assignments.len() % self.width == 0 {
            None
        } else {
            assignments.get(assignments.len() - 1)
        };

        let allowed_top = if let Some((i, o)) = top_constraint {
            let edge = self
                .tile_edges
                .get(i)
                .unwrap()
                .get(&Orientation(o.0, (o.1 + 2) % 4))
                .unwrap();

            // println!("top_edge: {}", edge);
            let reversed: String = edge.chars().rev().collect();

            let compatible: HashSet<(i32, Orientation)> =
                self.edges.get(&reversed).unwrap().iter().copied().collect();

            Some(compatible)
        } else {
            None
        };

        let allowed_left = if let Some((i, o)) = left_constraint {
            let edge = self
                .tile_edges
                .get(i)
                .unwrap()
                .get(&Orientation(o.0, (o.1 + 1) % 4))
                .unwrap();

            // println!("left_edge: {}", edge);
            let reversed: String = edge.chars().rev().collect();

            let compatible: HashSet<(i32, Orientation)> = self
                .edges
                .get(&reversed)
                .unwrap()
                .iter()
                .map(|(id, orientation)| (*id, Orientation(orientation.0, (orientation.1 + 1) % 4)))
                .collect();

            Some(compatible)
        } else {
            None
        };

        // println!("allowed_top: {:?}", &allowed_top);
        // println!("allowed_left: {:?}", &allowed_left);

        let allowed_by_constraints: Option<HashSet<(i32, Orientation)>> =
            if let Some(top) = allowed_top {
                allowed_left
                    .map(|left| left.intersection(&top).copied().collect())
                    .or(Some(top))
            } else {
                allowed_left
            };

        // println!("allowed_both: {:?}", &allowed_by_constraints);

        let allowed: HashSet<(i32, Orientation)> = if let Some(v) = allowed_by_constraints {
            let used: HashSet<i32> = assignments.iter().map(|(v, _)| *v).collect();
            v.into_iter().filter(|(v, _)| !used.contains(v)).collect()
        } else {
            self.tile_edges
                .iter()
                .flat_map(|(&id, edges)| {
                    edges
                        .iter()
                        .map(|(o, _)| (id, *o))
                        .collect::<Vec<(i32, Orientation)>>()
                })
                .collect()
        };

        // println!("allowed: {:?}", &allowed);

        for v in allowed {
            assignments.push(v);
            if let Some(v) = self.solve(&assignments) {
                return Some(v);
            }
            assignments.pop();
        }

        return None;
    }
}

fn assignments(input: &HashMap<i32, Tile>) -> Vec<(i32, Orientation)> {
    let mut tile_edges = HashMap::new();
    for (&id, tile) in input {
        tile_edges.insert(id, parse_edges(tile));
    }

    let mut edges: HashMap<String, Vec<(i32, Orientation)>> = HashMap::new();
    for (&id, orientation_edge) in &tile_edges {
        for (orientation, edge) in orientation_edge {
            edges
                .entry(edge.to_owned())
                .or_insert_with(|| Vec::new())
                .push((id, *orientation));
        }
    }

    for (id, map) in &tile_edges {
        println!("{}: {:?}", id, map);
    }

    for (id, map) in &edges {
        println!("{}: {:?}", id, map);
    }

    let width = (input.len() as f64).sqrt() as usize;
    let mut solver = Solver {
        tile_edges,
        edges,
        width,
        step: 0,
    };

    let assignments = Vec::new();
    let solution = solver.solve(&assignments);

    match solution {
        Some(v) => v,
        None => assignments,
    }
}

fn part1(assignments: &Vec<(i32, Orientation)>) -> i64 {
    let width = (assignments.len() as f64).sqrt() as usize;
    assignments[0].0 as i64
        * assignments[width - 1].0 as i64
        * assignments[assignments.len() - width].0 as i64
        * assignments[assignments.len() - 1].0 as i64
}

fn reconstruct(input: &HashMap<i32, Tile>, assignments: &Vec<(i32, Orientation)>) -> Tile {
    let width = (assignments.len() as f64).sqrt() as usize;
    println!("{:?}", assignments);
    let reconstructed: Vec<Tile> = assignments
        .iter()
        .map(|(id, o)| reconstruct_tile(&input[id], o))
        .collect();
    let tile_size = reconstructed[0].len() - 2;

    let mut big_tile = vec![vec!['.'; width * tile_size]; width * tile_size];

    for (i, tile) in reconstructed.iter().enumerate() {
        let x_offset = (i % width) * tile_size;
        let y_offset = (i / width) * tile_size;
        for (y, row) in tile[1..tile.len() - 1].iter().enumerate() {
            for (x, v) in row[1..row.len() - 1].iter().enumerate() {
                big_tile[y + y_offset][x + x_offset] = *v;
            }
        }
    }

    // print_tile(&big_tile);

    big_tile
}

fn part2(reconstructed: &Tile) -> i64 {
    let mut tile = reconstructed.to_owned();

    let pattern = indoc! {r"
                  # 
#    ##    ##    ###
 #  #  #  #  #  #    
    "
    };

    let positions: Vec<(usize, usize)> = pattern
        .split("\n")
        .enumerate()
        .flat_map(|(y, l)| {
            l.match_indices("#")
                .map(|v| (y, v.0))
                .collect::<Vec<(usize, usize)>>()
        })
        .collect();

    for flip in [Flip::None, Flip::Vertical, Flip::Horizontal].iter() {
        tile = flip_tile(&tile, flip);
        for _ in 0..4 {
            for y_offset in 0..tile.len() {
                for x_offset in 0..tile.len() {
                    let is_match = positions.iter().all(|(y, x)| {
                        match tile
                            .get(y + y_offset)
                            .and_then(|row| row.get(x + x_offset))
                            .unwrap_or(&'.')
                        {
                            '#' | 'O' => true,
                            _ => false,
                        }
                    });

                    if is_match {
                        positions
                            .iter()
                            .for_each(|(y, x)| tile[y + y_offset][x + x_offset] = 'O');
                    }
                }
            }

            tile = rotate_tile(&tile);
        }

        // Flip again to undo and prepare for next flip
        tile = flip_tile(&tile, flip);
    }

    // println!();
    // print_tile(&tile);

    tile.iter()
        .map(|r| r.iter().filter(|&&v| v == '#').count() as i64)
        .sum()
}

fn main() {
    let input = read_input();

    let assignments = assignments(&input);

    println!("part1: {}", part1(&assignments));

    let reconstructed = reconstruct(&input, &assignments);

    println!("part2: {}", part2(&reconstructed));
}

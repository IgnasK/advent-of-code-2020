use regex::Regex;
use std::collections::HashMap;
use std::fmt;
use std::io::{self, Read};

#[macro_use]
extern crate lazy_static;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

#[derive(Debug)]
struct ValidationError(String);
impl fmt::Display for ValidationError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "value failed validation: {}", self.0)
    }
}
impl std::error::Error for ValidationError {}

const REQUIRED_KEYS: [&str; 7] = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];

fn valid_passport(input: &str) -> bool {
    let parts: HashMap<&str, &str> = input
        .split_whitespace()
        .map(|part| {
            let split: Vec<&str> = part.split(":").collect();
            let key = split[0];
            let value = split[1];
            (key, value)
        })
        .collect();

    for key in &REQUIRED_KEYS {
        if !parts.contains_key(key) {
            return false;
        }
    }

    true
}

fn parse_integer(value: &str, min: i32, max: i32) -> Result<i32> {
    let parsed = value.parse::<i32>()?;

    if parsed < min || parsed > max {
        return Err(ValidationError(value.to_owned()).into());
    }

    Ok(parsed)
}

fn parse_height(value: &str) -> Result<()> {
    lazy_static! {
        static ref HEIGHT_REGEX: regex::Regex = Regex::new(r"^(\d+)(in|cm)$").unwrap();
    }

    let captures = HEIGHT_REGEX.captures(value).ok_or(ValidationError(value.to_owned()))?;

    let number = captures.get(1).unwrap().as_str();
    let unit = captures.get(2).unwrap().as_str();

    match unit {
        "cm" => parse_integer(number, 150, 193)?,
        "in" => parse_integer(number, 59, 76)?,
        _ => return Err(ValidationError(value.to_owned()).into()),
    };

    Ok(())
}

fn parse_colour(value: &str) -> Result<()> {
    lazy_static! {
        static ref COLOUR_REGEX: regex::Regex = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
    }

    if COLOUR_REGEX.is_match(value) {
        Ok(())
    } else {
        Err(ValidationError(value.to_owned()).into())
    }
}

static ALLOWED_EYE_COLOURS: [&str; 7] = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];

fn parse_eye_colour(value: &str) -> Result<()> {
    if ALLOWED_EYE_COLOURS.iter().any(|x| *x == value) {
        Ok(())
    } else {
        Err(ValidationError(value.to_owned()).into())
    }
}

fn parse_passport_id(value: &str) -> Result<()> {
    if value.len() == 9 && value.chars().all(|x| x.is_ascii_digit()) {
        Ok(())
    } else {
        Err(ValidationError(value.to_owned()).into())
    }
}

fn valid_passport_fields(input: &str) -> Result<()> {
    let parts: HashMap<&str, &str> = input
        .split_whitespace()
        .map(|part| {
            let split: Vec<&str> = part.split(":").collect();
            let key = split[0];
            let value = split[1];
            (key, value)
        })
        .collect();

        for key in &REQUIRED_KEYS {
            if !parts.contains_key(key) {
                return Err(ValidationError((*key).to_owned()).into());
            }
        }

    parse_integer(*parts.get("byr").unwrap(), 1920, 2002)?;
    parse_integer(*parts.get("iyr").unwrap(), 2010, 2020)?;
    parse_integer(*parts.get("eyr").unwrap(), 2020, 2030)?;
    parse_height(*parts.get("hgt").unwrap())?;
    parse_colour(*parts.get("hcl").unwrap())?;
    parse_eye_colour(*parts.get("ecl").unwrap())?;
    parse_passport_id(*parts.get("pid").unwrap())?;

    Ok(())
}

fn parse_input() -> Vec<String> {
    let mut stdin = String::new();
    io::stdin().read_to_string(&mut stdin).unwrap();

    stdin.split("\n\n").map(|x| String::from(x)).collect()
}

fn part1(input: &Vec<String>) -> i32 {
    input.iter().filter(|x| valid_passport(x)).count() as i32
}

fn part2(input: &Vec<String>) -> i32 {
    input.iter().filter(|x| {
        let valid = valid_passport_fields(x);

        match valid {
            Ok(_) => true,
            Err(err) => {
                println!("{}", err);
                false
            }
        }
    }
    ).count() as i32
}

fn main() {
    let input = parse_input();

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

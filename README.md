# Advent of Code 2020 in Rust


Here are my [https://adventofcode.com/2020](Advent of Code 2020) solutions written in Rust.

I have started this without having written a single line in Rust, so some solutions might be grossly inefficient or inelegant in Rust standards, but all of them work.


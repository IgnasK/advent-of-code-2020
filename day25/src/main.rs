use std::{
    collections::{HashMap, HashSet},
    io::BufRead,
};

fn read_input() -> (u64, u64) {
    let stdin = std::io::stdin();
    let mut iter = stdin.lock().lines();

    let a = iter.next().unwrap().unwrap().trim().parse().unwrap();
    let b = iter.next().unwrap().unwrap().trim().parse().unwrap();

    (a, b)
}

static MOD: u64 = 20_201_227;
static SQRT_MOD: u64 = 4_495;
static BASE: u64 = 7;

fn log(base: u64, v: u64) -> u64 {
    let mut table: HashMap<u64, u64> = HashMap::new();

    let mut power = 1u64;
    for i in 0..SQRT_MOD {
        table.insert(power, i);
        power = (power * base) % MOD;
    }

    let factor = exp(base, MOD - SQRT_MOD - 1);

    let mut remaining_power = v;
    for i in 0..SQRT_MOD {
        if let Some(v) = table.get(&remaining_power) {
            return i * SQRT_MOD + v;
        }
        remaining_power = (remaining_power * factor) % MOD;
    }

    0
}

fn exp(v: u64, power: u64) -> u64 {
    let mut result = 1;
    let mut v = v;
    let mut power = power;

    while power > 0 {
        if power % 2 == 1 {
            result = (result * v) % MOD;
        }
        v = (v * v) % MOD;

        power /= 2;
    }

    result
}

fn part1(input: &(u64, u64)) -> u64 {
    let power = log(BASE, input.0);

    let key = exp(input.1, power);

    key
}

fn main() {
    let input = read_input();
    println!("{:?}", &input);

    println!("part1: {}", part1(&input));
}

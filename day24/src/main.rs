use std::{
    collections::{HashMap, HashSet},
    io::BufRead,
};

#[derive(Debug)]
enum Direction {
    East,
    SouthEast,
    SouthWest,
    West,
    NorthWest,
    NorthEast,
}
type Description = Vec<Direction>;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Position(i32, i32);

impl std::ops::Add for &Position {
    type Output = Position;

    fn add(self, rhs: Self) -> Self::Output {
        Position(self.0 + rhs.0, self.1 + rhs.1)
    }
}

impl std::ops::AddAssign for Position {
    fn add_assign(&mut self, rhs: Self) {
        *self = Self(self.0 + rhs.0, self.1 + rhs.1);
    }
}

fn read_input() -> Vec<Description> {
    std::io::stdin()
        .lock()
        .lines()
        .map(|l| {
            let l = l.unwrap();

            let mut description = Description::new();

            let mut iter = l.trim().chars();
            while let Some(c) = iter.next() {
                let dir = match c {
                    'e' => Direction::East,
                    'w' => Direction::West,
                    's' => match iter.next() {
                        Some('e') => Direction::SouthEast,
                        Some('w') => Direction::SouthWest,
                        _ => panic!("unknown direction"),
                    },
                    'n' => match iter.next() {
                        Some('e') => Direction::NorthEast,
                        Some('w') => Direction::NorthWest,
                        _ => panic!("unknown direction"),
                    },
                    _ => panic!("unknown direction {}", c),
                };
                description.push(dir);
            }
            description
        })
        .collect()
}

/*
 -2-1 0 1 2 3
  -1 0 1 2 3
    0 1 2 3
*/

fn evaluate_description(description: &Description) -> Position {
    let mut pos = Position(0, 0);

    for dir in description {
        pos += match dir {
            Direction::East => Position(1, 0),
            Direction::SouthEast => Position(1, -1),
            Direction::SouthWest => Position(0, -1),
            Direction::West => Position(-1, 0),
            Direction::NorthEast => Position(0, 1),
            Direction::NorthWest => Position(-1, 1),
        }
    }

    pos
}

fn find_black_tiles(input: &Vec<Description>) -> HashSet<Position> {
    let mut flipped_tiles: HashMap<Position, i32> = HashMap::new();

    for tile in input {
        let pos = evaluate_description(tile);
        *flipped_tiles.entry(pos).or_insert(0) += 1;
    }

    flipped_tiles
        .into_iter()
        .filter_map(|(k, v)| if v % 2 == 1 { Some(k) } else { None })
        .collect()
}

const HEX_DIRECTIONS: [Position; 6] = [
    Position(1, 0),
    Position(1, -1),
    Position(0, -1),
    Position(-1, 0),
    Position(0, 1),
    Position(-1, 1),
];

fn step(black: &HashSet<Position>) -> HashSet<Position> {
    let mut adjacency: HashMap<Position, usize> = HashMap::new();

    for tile in black {
        for dir in HEX_DIRECTIONS.iter() {
            let adj_tile: Position = tile + dir;
            *adjacency.entry(adj_tile).or_insert(0) += 1;
        }
    }

    adjacency
        .into_iter()
        .filter_map(|(k, v)| {
            let was_black = black.contains(&k);
            if (v == 0 || v > 2) && was_black {
                None // Flipped to white
            } else if v == 2 && !was_black {
                Some(k) // Flipped to black
            } else if was_black {
                Some(k) // Remains black
            } else {
                None // remains white
            }
        })
        .collect()
}

fn part1(black: &HashSet<Position>) -> i32 {
    black.len() as i32
}

fn part2(initial: &HashSet<Position>) -> i32 {
    let mut black = initial.to_owned();

    for _ in 0..100 {
        black = step(&black);

        println!("{}", black.len());
    }

    black.len() as i32
}

fn main() {
    let input = read_input();

    let initial = find_black_tiles(&input);

    println!("part1: {}", part1(&initial));
    println!("part2: {}", part2(&initial));
}

use std::collections::HashMap;
use std::io::BufRead;

#[macro_use]
extern crate scan_fmt;

#[derive(Debug)]
struct Policy {
    min: i32,
    max: i32,
    letter: char,
}

fn read_input() -> Vec<(Policy, String)> {
    let mut result: Vec<(Policy, String)> = Vec::new();

    let stdin = std::io::stdin();
    for line in stdin.lock().lines() {
        let (min, max, letter, word) =
            scan_fmt!(&line.unwrap(), "{}-{} {}: {}", i32, i32, char, String).unwrap();
        result.push((Policy { min, max, letter }, word))
    }

    result
}

fn parse_frequency(s: &String) -> HashMap<char, i32> {
    let mut result = HashMap::new();

    for c in s.chars() {
        *result.entry(c).or_insert(0) += 1;
    }

    result
}

fn part1(input: &Vec<(Policy, String)>) -> i32 {
    let result = input
        .iter()
        .filter(|x| {
            let policy = &x.0;
            let frequencies = parse_frequency(&x.1);
            let f = *frequencies.get(&policy.letter).unwrap_or(&0);

            f >= policy.min && f <= policy.max
        })
        .count();

    result as i32
}

fn part2(input: &Vec<(Policy, String)>) -> i32 {
    let result = input
        .iter()
        .filter(|x| {
            let policy = &x.0;

            let first = x.1.chars().nth((policy.min - 1) as usize).unwrap();
            let second = x.1.chars().nth((policy.max - 1) as usize).unwrap();

            (first == policy.letter) ^ (second == policy.letter)
        })
        .count();

    result as i32
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

use std::{collections::HashMap, io::BufRead, num::Wrapping};

#[macro_use] extern crate scan_fmt;

#[derive(Debug, Copy, Clone)]
struct Mask {
    and: u64, or: u64, unchanged: u64
}
#[derive(Debug)]
enum Command {
    ChangeMask(String),
    SetMem { addr: u64, value: u64 },
}

fn mask_to_command(mask: &str) -> Mask {
    let mut or: u64 = 0;
    let mut and: u64 = !0;
    let mut unchanged: u64 = 0;

    for c in mask.chars() {
        or <<= 1;
        and <<= 1;
        and |= 1;
        unchanged <<= 1;
        match c {
            '1' => or |= 1,
            '0' => and &= !1,
            'X' => unchanged |= 1,
            _ => (),
        }
    }

    Mask{
        and,
        or,
        unchanged,
    }
}

fn read_input() -> Vec<Command> {

    std::io::stdin().lock().lines().map(|l| {
        let line = l.unwrap();
        if let Ok(mask) = scan_fmt!(&line, "mask = {}", String) {
            Command::ChangeMask(mask)
        } else if let Ok((addr, value)) = scan_fmt!(&line, "mem[{d}] = {}", u64, u64) {
            Command::SetMem{
                addr: addr,
                value: value,
            }
        } else {
            panic!("bad line {}", &line)
        }
    }).collect()
}

fn part1(input: &Vec<Command>) -> u64 {
    let mut memory: HashMap<u64, u64> = HashMap::new();
    let mut mask = Mask{and: 0, or: 0, unchanged: 0};

    for command in input {
        match command {
            Command::ChangeMask(m) => mask = mask_to_command(m),
            Command::SetMem{addr, value} => { memory.insert(*addr, *value & mask.and | mask.or); },
        }
    }

    let sum = memory.values().sum();

    sum
}

fn part2(input: &Vec<Command>) -> u64 {
    let mut memory: HashMap<u64, u64> = HashMap::new();
    let mut mask = Mask{and: 0, or: 0, unchanged: 0};

    for command in input {
        match command {
            Command::ChangeMask(m) => mask = mask_to_command(m),
            Command::SetMem{addr, value} => {
                // println!("{}:", &addr);
                let mut unpred_bits = Wrapping(!mask.unchanged);
                while unpred_bits.0 != 0 {
                    unpred_bits.0 |= !mask.unchanged;

                    let masked_addr = (*addr & !mask.unchanged) | (unpred_bits.0 & mask.unchanged) | mask.or;
                    // println!("{:#036b}: {:#036b}", &unpred_bits, &masked_addr);
                    memory.insert(masked_addr, *value);

                    unpred_bits += Wrapping(1u64);
                }
            },
        }
    }

    let sum = memory.values().sum();

    sum
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part1: {}", part2(&input));
}

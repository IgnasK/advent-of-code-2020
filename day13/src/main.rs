use std::io;

use ring_algorithm::{chinese_remainder_theorem, gcd};

#[derive(Debug)]
struct Input {
    time: i64,
    buses: Vec<Option<i64>>,
}

fn read_input() -> Input {
    let stdin = io::stdin();

    let mut time_buffer = String::new();
    stdin.read_line(&mut time_buffer).unwrap();
    let time = time_buffer.trim().parse::<i64>().unwrap();

    
    let mut bus_buffer = String::new();
    stdin.read_line(&mut bus_buffer).unwrap();
    let buses = bus_buffer.trim().split(",").map(|s| s.parse().ok()).collect();

    Input{
        time,
        buses,
    }
}

fn part1(input: &Input) -> i64 {
    let mut best_bus = 0;
    let mut best_time = -1;

    for bus in input.buses.iter().filter_map(|&x| x) {
        
        // Double modulus to account for when bus departs at input.time
        let wait_time = ((-input.time) % bus + bus) % bus;

        if wait_time < best_time || best_time == -1 {
            best_time = wait_time;
            best_bus = bus;
        }
    }

    return best_time * best_bus;
}

fn lcm(values: &Vec<i64>) -> i64 {
    let mut result = 1;
    for v in values {
        result = result * *v / gcd(result, *v);
    }

    result
}

fn part2(input: &Input) -> i64 {
    let mut remainders: Vec<i64> = vec![];
    let mut modulus: Vec<i64> = vec![];

    for (index, &bus) in input.buses.iter().enumerate() {
        if let Some(v) = bus {
            remainders.push((v - index as i64) % v);
            modulus.push(v);
        }
    }

    println!("{:?} {:?}", &remainders, &modulus);

    let result = chinese_remainder_theorem(&remainders, &modulus).unwrap();
    let period = lcm(&modulus);

    ((result % period) + period) % period
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

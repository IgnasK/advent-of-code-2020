use std::io::{self, BufRead};

fn read_input() -> Vec<i32> {
    io::stdin()
        .lock()
        .lines()
        .map(|l| l.unwrap().parse().unwrap())
        .collect()
}

fn part1(sorted: &Vec<i32>) -> i32 {
    let mut differences = vec![0; 4];
    let mut source = 0;
    for output in sorted {
        let difference = (*output - source) as usize;
        if difference > 3 {
            panic!("too large difference {}", difference);
        }

        differences[difference] += 1;

        source = *output;
    }

    differences[1] * differences[3]
}

fn part2(sorted: &Vec<i32>) -> i64 {
    let mut combinations: Vec<i64> = vec![];
    for (i, output) in sorted.iter().enumerate() {
        combinations.push(0);

        let min_candidate = std::cmp::max(i as i32 - 3, 0) as usize;
        for j in (min_candidate..i).rev() {
            let difference = *output - sorted[j];
            if difference <= 3 {
                combinations[i] += combinations[j];
            }
        }

        if *output <= 3 {
            // Also, we can just connect to ground
            combinations[i] += 1;
        }
    }

    *combinations.last().unwrap()
}

fn main() {
    let mut input = read_input();
    input.sort();
    input.push(input.last().unwrap() + 3); // built-in adapter

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

use lazy_static::*;
use regex::Regex;
use scan_fmt::*;
use std::{collections::HashMap, io::Read};

#[derive(Debug)]
struct Input {
    expr: String,
    messages: Vec<String>,
}
#[derive(Debug)]
enum Rule {
    Or(Vec<Vec<usize>>),
    Literal(char),
}

lazy_static! {
    static ref RULE_REGEX: Regex = Regex::new(r"^(\d+): (.+)$").unwrap();
}

const MAX_DEPTH: i32 = 14;

fn build_regex(id: usize, rules: &HashMap<usize, Rule>, depth: i32) -> String {
    let rule = rules.get(&id).unwrap();

    // Short circuit early for part 2, anything above 14 works
    if depth > MAX_DEPTH {
        return "x".to_owned();
    }

    match rule {
        Rule::Literal(c) => c.to_string(),
        Rule::Or(expr) => {
            let mid = expr
                .iter()
                .map(|subexpr| {
                    subexpr
                        .iter()
                        .map(|&id| build_regex(id, rules, depth + 1))
                        .collect::<Vec<String>>()
                        .join("")
                })
                .collect::<Vec<String>>()
                .join("|");

            format!("({})", mid)
        }
    }
}

fn build_regex_rules(rule_input: &str) -> HashMap<usize, Rule> {
    let mut rules: HashMap<usize, Rule> = HashMap::new();

    for line in rule_input.split("\n") {
        let matches = RULE_REGEX.captures(line).unwrap();

        let id = matches.get(1).unwrap().as_str().parse::<usize>().unwrap();
        let body = matches.get(2).unwrap().as_str();

        if let Ok(c) = scan_fmt!(body, "\"{[a-z]}\"", char) {
            rules.insert(id, Rule::Literal(c));
        } else {
            let expr: Vec<Vec<usize>> = body
                .split(" | ")
                .map(|expr| {
                    expr.split(" ")
                        .map(|v| v.parse::<usize>().unwrap())
                        .collect()
                })
                .collect();
            rules.insert(id, Rule::Or(expr));
        }
    }

    rules
}

fn read_input() -> Input {
    let mut buffer = String::new();
    std::io::stdin().lock().read_to_string(&mut buffer).unwrap();

    let mut iter = buffer.split("\n\n");
    let rules = iter.next().unwrap();
    let messages: Vec<String> = iter
        .next()
        .unwrap()
        .split("\n")
        .map(|v| v.to_owned())
        .collect();

    let rules = build_regex_rules(rules);
    let regex = build_regex(0, &rules, 0);

    Input {
        expr: regex,
        messages,
    }
}

fn part1(input: &Input) -> usize {
    let regex = Regex::new(&format!("^{}$", input.expr)).unwrap();
    input.messages.iter().filter(|&f| regex.is_match(f)).count()
}
fn main() {
    let input = read_input();

    println!("{:?}", &input.expr);
    println!("solution: {}", part1(&input));
}

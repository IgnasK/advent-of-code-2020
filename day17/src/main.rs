use std::collections::{HashMap, HashSet};
use std::io::BufRead;

#[derive(PartialEq, Eq, Debug, Hash, Copy, Clone)]
struct Pos(i32, i32, i32, i32);

fn read_input() -> HashSet<Pos> {
    let mut result = HashSet::new();

    for (y, line) in std::io::stdin().lock().lines().enumerate() {
        let line = line.unwrap();

        for (x, c) in line.trim().chars().enumerate() {
            match c {
                '#' => {
                    result.insert(Pos(x as i32, y as i32, 0, 0));
                }
                _ => (),
            }
        }
    }

    result
}

fn step(world: &HashSet<Pos>, dimensions: i32) -> HashSet<Pos> {
    let mut candidates: HashMap<Pos, i32> = HashMap::new();

    for pos in world {
        let w_range = if dimensions == 4 { -1..=1 } else { 0..=0 };
        for dw in w_range {
            for dz in -1..=1 {
                for dy in -1..=1 {
                    for dx in -1..=1 {
                        if dx == 0 && dy == 0 && dz == 0 && dw == 0 {
                            continue;
                        }

                        let candidate = Pos(pos.0 + dx, pos.1 + dy, pos.2 + dz, pos.3 + dw);

                        *candidates.entry(candidate).or_insert(0) += 1;
                    }
                }
            }
        }
    }

    let mut result = HashSet::new();

    for (candidate, count) in candidates {
        if count == 3 || (count == 2 && world.contains(&candidate)) {
            result.insert(candidate);
        }
    }

    result
}

fn solve(input: &HashSet<Pos>, dimensions: i32) -> i32 {
    let mut world = input.to_owned();

    for _ in 0..6 {
        let next = step(&world, dimensions);
        // println!("{:?}", &next);
        world = next;
    }

    return world.len() as i32;
}

fn main() {
    let input = read_input();
    println!("{:?}", &input);

    println!("part1: {}", solve(&input, 3));
    println!("part2: {}", solve(&input, 4));
}

use std::cmp::{max, min};
use std::{collections::HashSet, collections::VecDeque, io::Read};

type Stack = VecDeque<i32>;
type Game = (Stack, Stack);

fn read_input() -> Game {
    let mut buffer = String::new();
    std::io::stdin().lock().read_to_string(&mut buffer).unwrap();

    let mut stacks = buffer.trim().split("\n\n").map(|s| {
        s.split("\n")
            .skip(1)
            .map(|l| l.trim().parse::<i32>().unwrap())
            .collect::<Stack>()
    });

    (stacks.next().unwrap(), stacks.next().unwrap())
}

fn play(input: &Game) -> Stack {
    let mut a_queue = input.0.clone();
    let mut b_queue = input.1.clone();

    while a_queue.len() > 0 && b_queue.len() > 0 {
        let a = a_queue.pop_front().unwrap();
        let b = b_queue.pop_front().unwrap();

        if a > b {
            a_queue.push_back(max(a, b));
            a_queue.push_back(min(a, b));
        } else {
            b_queue.push_back(max(a, b));
            b_queue.push_back(min(a, b));
        }
    }

    if !a_queue.is_empty() {
        a_queue
    } else {
        b_queue
    }
}

fn play_recursive(input: &Game) -> (i32, Stack) {
    let mut seen = HashSet::new();

    let mut a_queue = input.0.clone();
    let mut b_queue = input.1.clone();

    while a_queue.len() > 0 && b_queue.len() > 0 {
        let state = (a_queue.clone(), b_queue.clone());
        if seen.contains(&state) {
            return (0, a_queue);
        }
        seen.insert(state);

        let a = a_queue.pop_front().unwrap();
        let b = b_queue.pop_front().unwrap();

        if a as usize <= a_queue.len() && b as usize <= b_queue.len() {
            let (winner, _) = play_recursive(&(
                a_queue.iter().take(a as usize).cloned().collect(),
                b_queue.iter().take(b as usize).cloned().collect(),
            ));
            if winner == 0 {
                a_queue.push_back(a);
                a_queue.push_back(b);
            } else {
                b_queue.push_back(b);
                b_queue.push_back(a);
            }
        } else {
            if a > b {
                a_queue.push_back(max(a, b));
                a_queue.push_back(min(a, b));
            } else {
                b_queue.push_back(max(a, b));
                b_queue.push_back(min(a, b));
            }
        }
    }

    if !a_queue.is_empty() {
        (0, a_queue)
    } else {
        (1, b_queue)
    }
}

fn score(result: Stack) -> i64 {
    let mut score = 0;

    for (i, v) in result.into_iter().rev().enumerate() {
        score += ((i + 1) as i64) * (v as i64)
    }

    score
}

fn part1(input: &Game) -> i64 {
    let result = play(input);

    score(result)
}

fn part2(input: &Game) -> i64 {
    let result = play_recursive(input);

    score(result.1)
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

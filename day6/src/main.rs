use itertools::{Itertools, Position};
use std::{
    collections::HashSet,
    io::{self, Read},
};

type Person = String;
type Group = Vec<Person>;

fn read_input() -> Vec<Group> {
    let mut file = String::new();
    io::stdin().read_to_string(&mut file).unwrap();

    file.trim()
        .split("\n\n")
        .map(|x| x.split('\n').map(|s| s.to_owned()).collect())
        .collect()
}

fn count_answers(group: &Group) -> i32 {
    let mut answers: HashSet<char> = HashSet::new();

    for person in group {
        for answer in person.chars() {
            answers.insert(answer);
        }
    }

    answers.len() as i32
}

fn count_mutual_answers(group: &Group) -> i32 {
    let mut answers: HashSet<char> = HashSet::new();

    for person in group.iter().with_position() {
        match person {
            Position::First(s) | Position::Only(s) => {
                answers = s.chars().collect();
            }
            Position::Middle(s) | Position::Last(s) => {
                let set: HashSet<char> = s.chars().collect();
                answers = answers.intersection(&set).cloned().collect();
            }
        }
    }

    let result = answers.len() as i32;

    println!("{:?}: {}", group, result);

    result
}

fn part1(input: &Vec<Group>) -> i32 {
    input.iter().map(|g| count_answers(g)).sum()
}

fn part2(input: &Vec<Group>) -> i32 {
    input.iter().map(|g| count_mutual_answers(g)).sum()
}

fn main() {
    let input = read_input();
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

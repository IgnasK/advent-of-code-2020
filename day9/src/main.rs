use std::{collections::HashSet, io::BufRead};

fn read_input() -> Vec<i64> {
    std::io::stdin()
        .lock()
        .lines()
        .map(|line| line.unwrap().parse::<i64>().unwrap())
        .collect()
}

fn read_buf_size() -> i64 {
    let args: Vec<_> = std::env::args().collect();
    args.get(1)
        .and_then(|x| x.parse().ok())
        .expect("provide buffer size (e.g. 25)")
}

fn set_has_value(set: &HashSet<i64>, target: i64) -> bool {
    for a in set {
        let b = target - *a;
        if set.contains(&b) {
            return true;
        }
    }
    false
}

fn part1(buffer: i64, input: &Vec<i64>) -> i64 {
    let mut set: HashSet<i64> = HashSet::new();

    for (i, value) in input.iter().enumerate() {
        if set.len() == buffer as usize {
            if !set_has_value(&set, *value) {
                return *value;
            }

            set.remove(&input[i - buffer as usize]);
        }
        set.insert(*value);
    }

    unreachable!()
}

fn part2(target: i64, input: &Vec<i64>) -> i64 {
    let mut start = 0;
    let mut end = 0;

    let mut sum = 0;

    while end < input.len() {
        sum += input[end];
        end += 1;

        while sum > target {
            sum -= input[start];
            start += 1;
        }

        if sum == target && end - start > 1 {
            let numbers = &input[start..end];
            let min = numbers.iter().min().unwrap();
            let max = numbers.iter().max().unwrap();

            return *min + *max;
        }
    }

    0
}

fn main() {
    let buffer = read_buf_size();
    let input = read_input();

    let target = part1(buffer, &input);
    println!("part1: {}", &target);
    println!("part2: {}", part2(target, &input));
}

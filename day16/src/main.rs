use std::{
    cmp::{max, min},
    collections::HashSet,
};
use std::{collections::HashMap, io::Read};

#[derive(Debug)]
struct Range(i32, i32);

impl Range {
    fn combine(&self, other: &Self) -> Self {
        Self(min(self.0, other.0), max(self.1, other.1))
    }
}
#[derive(Debug)]
struct Rule(String, Vec<Range>);
type Ticket = Vec<i32>;
#[derive(Debug)]
struct Input {
    rules: Vec<Rule>,
    my_ticket: Ticket,
    tickets: Vec<Ticket>,
}

fn read_input() -> Input {
    let mut buffer = String::new();
    std::io::stdin().lock().read_to_string(&mut buffer).unwrap();

    let subbuffers: Vec<&str> = buffer.split("\n\n").collect();
    let (rules, my_ticket, tickets) = (subbuffers[0], subbuffers[1], subbuffers[2]);

    let parse_ticket = |l: &str| {
        l.trim()
            .split(",")
            .map(|v| v.parse::<i32>().unwrap())
            .collect::<Ticket>()
    };

    Input {
        rules: rules
            .split("\n")
            .map(|l| {
                let mut iter = l.split(": ");
                let rule_name = iter.next().unwrap();
                let conditions = iter.next().unwrap();
                let ranges = conditions
                    .split(" or ")
                    .map(|c| {
                        let values: Vec<i32> =
                            c.trim().split("-").map(|v| v.parse().unwrap()).collect();
                        Range(values[0], values[1])
                    })
                    .collect();
                Rule(rule_name.to_owned(), ranges)
            })
            .collect(),
        my_ticket: parse_ticket(my_ticket.split("\n").nth(1).unwrap()),
        tickets: tickets
            .trim()
            .split("\n")
            .skip(1)
            .map(parse_ticket)
            .collect(),
    }
}

fn valid_value(rules: &[Rule], value: i32) -> bool {
    rules
        .iter()
        .any(|r| r.1.iter().any(|r| value >= r.0 && value <= r.1))
}

fn valid_values(rule: &Rule, values: &[i32]) -> bool {
    values
        .iter()
        .all(|&v| rule.1.iter().any(|r| v >= r.0 && v <= r.1))
}

fn part1(input: &Input) -> i32 {
    let mut result = 0;

    for ticket in &input.tickets {
        result += ticket
            .iter()
            .filter(|&&v| !valid_value(&input.rules, v))
            .sum::<i32>();
    }

    result
}

fn allowed_assignments(rules: &Vec<Rule>, ranges: &Vec<Vec<i32>>) -> Vec<(String, Vec<usize>)> {
    let mut result: Vec<(String, Vec<usize>)> = rules
        .iter()
        .map(|r| {
            (
                r.0.to_owned(),
                ranges
                    .iter()
                    .enumerate()
                    .filter_map(|(i, values)| {
                        if valid_values(r, &values) {
                            Some(i)
                        } else {
                            None
                        }
                    })
                    .collect(),
            )
        })
        .collect();

    result.sort_by_key(|r| r.1.len());

    result
}

// This assumes every solved variable relaxes another condition to be solvable
fn sat(input: Vec<(String, Vec<usize>)>) -> HashMap<String, usize> {
    let mut result = HashMap::new();
    let mut used: HashSet<usize> = HashSet::new();

    for (rule, allowed) in input {
        let assigned = allowed.iter().find(|&i| !used.contains(i)).unwrap();
        result.insert(rule, *assigned);
        used.insert(*assigned);
    }

    result
}

fn part2(input: &Input) -> i64 {
    let valid_tickets: Vec<&Ticket> = input
        .tickets
        .iter()
        .filter(|ticket| ticket.iter().all(|&v| valid_value(&input.rules, v)))
        .collect();

    let mut ranges: Vec<Vec<i32>> = vec![Vec::new(); input.my_ticket.len()];
    for ticket in valid_tickets {
        for (i, &val) in ticket.iter().enumerate() {
            ranges[i].push(val);
        }
    }

    let allowed_assignments = allowed_assignments(&input.rules, &ranges);
    println!("{:?}", &allowed_assignments);

    let assignments = sat(allowed_assignments);
    println!("{:?}", &assignments);

    assignments
        .iter()
        .filter_map(|(name, &i)| {
            if name.starts_with("departure") {
                Some(input.my_ticket[i] as i64)
            } else {
                None
            }
        })
        .product()
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

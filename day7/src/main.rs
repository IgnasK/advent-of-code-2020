use std::collections::{HashMap, HashSet};
use std::io::{self, BufRead};

use regex::Regex;

#[macro_use]
extern crate lazy_static;

type BagType = String;
type BagContents = HashMap<BagType, i32>;
type Bags = HashMap<BagType, BagContents>;

lazy_static! {
    static ref LINE_REGEX: Regex = Regex::new(r"^(.+) bags contain (.+).$").unwrap();
    static ref CONTENTS_REGEX: Regex = Regex::new(r"^(\d+) (.+) bags?$").unwrap();
}

fn parse_bag_contents(input: &str) -> BagContents {
    if input == "no other bags" {
        return BagContents::new();
    }

    input
        .split(", ")
        .map(|contents| {
            let captures = CONTENTS_REGEX.captures(contents).unwrap();

            let count = captures.get(1).unwrap().as_str().parse::<i32>().unwrap();
            let bag = captures.get(2).unwrap().as_str().to_owned();

            (bag, count)
        })
        .collect()
}

fn read_input() -> Bags {
    let mut result = Bags::new();

    for line in io::stdin().lock().lines() {
        let line = line.unwrap();

        let captures = LINE_REGEX.captures(&line).unwrap();
        let bag = captures.get(1).unwrap().as_str().to_owned();
        let contents = captures.get(2).unwrap().as_str();

        result.insert(bag, parse_bag_contents(contents));
    }

    result
}

fn invert(bags: &Bags) -> Bags {
    let mut result = Bags::new();

    for (bag, contents) in bags {
        for (inner, count) in contents {
            let entry = result.entry(inner.to_owned()).or_insert(BagContents::new());
            entry.insert(bag.to_owned(), *count);
        }
    }

    result
}

fn part1(input: &Bags) -> i32 {
    // Will find a closure of inverted directed graph
    let graph = invert(input);
    let mut visited: HashSet<&str> = HashSet::new();
    let mut stack: Vec<&str> = Vec::new();

    stack.push("shiny gold");

    while !stack.is_empty() {
        let node = stack.pop().unwrap();

        if !graph.contains_key(node) {
            continue;
        }

        for neighbour in graph.get(node).unwrap().keys() {
            if visited.contains(neighbour.as_str()) {
                continue;
            }

            visited.insert(neighbour);
            stack.push(neighbour);
        }
    }

    visited.len() as i32
}

fn part2(graph: &Bags) -> i32 {
    let mut sizes: HashMap<&str, i32> = HashMap::new();
    let mut stack: Vec<&str> = Vec::new();

    stack.push("shiny gold");

    while !stack.is_empty() {
        let node = *stack.last().unwrap();

        if !graph.contains_key(node) {
            stack.pop();
            continue;
        }

        if graph.contains_key(node) {
            for neighbour in graph.get(node).unwrap().keys() {
                if sizes.contains_key(neighbour.as_str()) {
                    continue;
                }

                sizes.insert(neighbour.as_str(), 0);
                stack.push(neighbour);
            }
        }

        if *stack.last().unwrap() == node {
            let mut size = 0;
            for (neighbour, count) in graph.get(node).unwrap() {
                size += (sizes[neighbour.as_str()] + 1) * count;
            }

            sizes.insert(node, size);
            stack.pop();
        }
    }

    sizes["shiny gold"]
}

fn main() {
    let input = read_input();
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

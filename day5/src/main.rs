use itertools::{Itertools, Position};
use std::io::{self, BufRead};

fn read_input() -> Vec<String> {
    io::stdin().lock().lines().map(|x| x.unwrap()).collect()
}

// Parse binary to int
fn seat_id(steps: &str) -> i32 {
    let mut result = 0;

    for step in steps.chars() {
        result *= 2;
        if step == 'R' || step == 'B' {
            result += 1;
        }
    }

    result
}

fn steps_from_seat_id(id: i32) -> String {
    let mut result = ['\0'; 10];

    let mut row = id / 8;
    let mut column = id % 8;

    for i in (7..10).rev() {
        result[i] = if column % 2 == 1 { 'R' } else { 'L' };
        column /= 2;
    }

    for i in (0..7).rev() {
        result[i] = if row % 2 == 1 { 'B' } else { 'F' };
        row /= 2;
    }

    println!("{:?}", &result);

    result.iter().collect()
}

fn part1(input: &Vec<String>) -> i32 {
    input
        .iter()
        .map(|steps| {
            let id = seat_id(steps);
            println!("{}: {}", steps, id);
            id
        })
        .fold(0, |l, r| std::cmp::max(l, r))
}

fn part2(input: &Vec<String>) -> i32 {
    let mut used_seats: Vec<i32> = input.iter().map(|steps| seat_id(steps)).collect();

    used_seats.sort();

    let mut next_available = 0;
    for seat in used_seats.iter().with_position() {
        match seat {
            Position::First(id) => next_available = id + 1,
            Position::Middle(id) => {
                if *id != next_available {
                    return next_available;
                }
                next_available = id + 1;
            }
            _ => (),
        }
    }

    0
}

fn main() {
    let input = read_input();
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

use std::io::BufRead;

#[derive(Debug, Copy, Clone)]
enum Symbol {
    Num(i64),
    Add,
    Multiply,
    OpenParen,
    CloseParen,
}

type Expr = Vec<Symbol>;

fn read_input() -> Vec<Expr> {
    std::io::stdin()
        .lock()
        .lines()
        .map(|l| {
            let line = l.unwrap();

            let mut expr = Expr::new();

            for c in line.chars() {
                match c {
                    ' ' => (),
                    '+' => expr.push(Symbol::Add),
                    '*' => expr.push(Symbol::Multiply),
                    '(' => expr.push(Symbol::OpenParen),
                    ')' => expr.push(Symbol::CloseParen),
                    '0'..='9' => {
                        let digit = c.to_digit(10).unwrap() as i64;
                        if let Some(Symbol::Num(v)) = expr.last() {
                            *expr.last_mut().unwrap() = Symbol::Num(v * 10 + digit);
                        } else {
                            expr.push(Symbol::Num(digit))
                        }
                    }
                    _ => panic!("unhandled symbol {}", c),
                }
            }

            expr
        })
        .collect()
}

fn precedence(s: &Symbol) -> i32 {
    match s {
        &Symbol::Num(_) => 0,
        &Symbol::Add | &Symbol::Multiply => 1,
        &Symbol::OpenParen | &Symbol::CloseParen => -1,
    }
}

fn precedence_part2(s: &Symbol) -> i32 {
    match s {
        &Symbol::Num(_) => 0,
        &Symbol::Multiply => 1,
        &Symbol::Add => 2,
        &Symbol::OpenParen | &Symbol::CloseParen => -1,
    }
}

fn eval(e: &Expr, precedence: &dyn Fn(&Symbol) -> i32) -> i64 {
    let mut numbers: Vec<i64> = Vec::new();
    let mut operators = Expr::new();

    let eval_symbol = |mut numbers: Vec<i64>, s| {
        match s {
            Symbol::Add => {
                let a = numbers.pop().unwrap();
                let b = numbers.pop().unwrap();
                numbers.push(a + b)
            }
            Symbol::Multiply => {
                let a = numbers.pop().unwrap();
                let b = numbers.pop().unwrap();
                numbers.push(a * b)
            }
            _ => panic!("unable to evaluate symbol {:?}", s),
        }
        numbers
    };

    for &s in e {
        match s {
            Symbol::Num(v) => numbers.push(v),
            Symbol::Add | Symbol::Multiply => {
                while let Some(v) = operators.last() {
                    if precedence(v) >= precedence(&s) {
                        numbers = eval_symbol(numbers, operators.pop().unwrap());
                    } else {
                        break;
                    }
                }
                operators.push(s);
            }
            Symbol::OpenParen => operators.push(s),
            Symbol::CloseParen => {
                while let Some(v) = operators.pop() {
                    if let Symbol::OpenParen = v {
                        break;
                    }
                    numbers = eval_symbol(numbers, v);
                }
            }
        }
        // println!("{:?}: {:?}, {:?}", s, &numbers, &operators);
    }

    while let Some(v) = operators.pop() {
        numbers = eval_symbol(numbers, v);
        // println!("_: {:?}, {:?}", &numbers, &operators);
    }

    numbers.pop().unwrap()
}

fn part1(input: &Vec<Expr>) -> i64 {
    input.iter().map(|v| eval(v, &precedence)).sum()
}

fn part2(input: &Vec<Expr>) -> i64 {
    input.iter().map(|v| eval(v, &precedence_part2)).sum()
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part1: {}", part2(&input));
}

use std::io::{self, BufRead};

struct Slope {
    dx: usize,
    dy: usize,
}

type Map = Vec<Vec<char>>;

fn read_input() -> Map {
    let mut result = Vec::new();

    for line in io::stdin().lock().lines() {
        let linestr = line.unwrap();
        let linevec: Vec<char> = linestr.trim().chars().collect();

        result.push(linevec)
    }

    result
}

fn count_trees(map: &Map, slope: &Slope) -> i32 {
    let height = map.len();
    let width = map[0].len();

    let mut result = 0;

    let mut x = slope.dx;
    let mut y = slope.dy;

    while y < height {
        let c = map[y][x % width];

        if c == '#' {
            result += 1;
        }

        x += slope.dx;
        y += slope.dy;
    }

    result
}

fn part1(map: &Map) -> i32 {
    let slope = Slope { dx: 3, dy: 1 };
    count_trees(map, &slope)
}

fn part2(map: &Map) -> i64 {
    let slopes = [
        Slope { dx: 1, dy: 1 },
        Slope { dx: 3, dy: 1 },
        Slope { dx: 5, dy: 1 },
        Slope { dx: 7, dy: 1 },
        Slope { dx: 1, dy: 2 },
    ];

    slopes
        .iter()
        .map(|slope| count_trees(map, slope) as i64)
        .fold(1, |res, val| res * val)
}

fn main() {
    let input = read_input();
    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

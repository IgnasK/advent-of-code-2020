use std::{collections::HashMap, io::Read};

fn read_input() -> Vec<i32> {
    let mut buffer = String::new();
    std::io::stdin().lock().read_to_string(&mut buffer).unwrap();

    buffer.trim().split(",").map(|v| v.parse().unwrap()).collect()
}

fn run(input: &Vec<i32>, target: i32) -> i32 {
    let mut history: HashMap<i32, i32> = HashMap::new();
    let mut time = 0;
    let mut next = 0;

    for &v in input {
        match history.insert(v, time) {
            Some(old) => next = time - old,
            None => next = 0,
        }

        time += 1
    }

    while time < target - 1 {
        match history.insert(next, time) {
            Some(old) => next = time - old,
            None => next = 0,
        }

        time += 1
    }

    next
}

fn main() {
    let input = read_input();

    println!("part1: {}", run(&input, 2020));
    println!("part2: {}", run(&input, 30000000));
}

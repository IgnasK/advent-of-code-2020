use std::io::{self, BufRead};

type Map = Vec<Vec<u8>>;

#[allow(dead_code)]
fn display_map(map: &Map) {
    for row in map {
        println!("{}", std::str::from_utf8(row).unwrap());
    }
    println!("");
}

fn read_input() -> Map {
    io::stdin()
        .lock()
        .lines()
        .map(|l| l.unwrap().trim().into())
        .collect()
}

fn count_occupied(map: &Map) -> i32 {
    map.iter()
        .map(|row| row.iter().filter(|&&v| v == b'#').count())
        .sum::<usize>() as i32
}

fn count_occupied_surrounding(map: &Map, x: i32, y: i32) -> i32 {
    let is_occupied = |x: i32, y: i32| {
        if x < 0 || y < 0 {
            return false;
        }
        let value = *map
            .get(y as usize)
            .and_then(|row| row.get(x as usize))
            .unwrap_or(&b'.');

        value == b'#'
    };

    let mut result = 0;

    for ty in y - 1..=y + 1 {
        for tx in x - 1..=x + 1 {
            if tx == x && ty == y {
                continue;
            }

            result += is_occupied(tx, ty) as i32
        }
    }

    result
}

fn count_occupied_visible(map: &Map, x: i32, y: i32) -> i32 {
    let get_seat = |x: i32, y: i32| {
        if x < 0 || y < 0 {
            return b'L';
        }
        *map.get(y as usize)
            .and_then(|row| row.get(x as usize))
            .unwrap_or(&b'L')
    };

    let mut result = 0;

    for dy in -1..=1 {
        for dx in -1..=1 {
            if dx == 0 && dy == 0 {
                continue;
            }

            let mut tx = x;
            let mut ty = y;
            loop {
                tx += dx;
                ty += dy;
                let c = get_seat(tx, ty);
                match c {
                    b'#' => {
                        result += 1;
                        break;
                    }
                    b'L' => break,
                    _ => (),
                }
            }
        }
    }

    result
}

fn step<F>(map: &Map, rule: F) -> Map
where
    F: Fn(&Map, i32, i32, u8) -> u8,
{
    // Performs an unnecessary big allocation on every step, but it works

    map.iter()
        .enumerate()
        .map(|(y, row)| {
            row.iter()
                .enumerate()
                .map(|(x, val)| rule(map, x as i32, y as i32, *val))
                .collect()
        })
        .collect()
}

fn part1_rule(map: &Map, x: i32, y: i32, val: u8) -> u8 {
    match &val {
        b'#' | b'L' => {
            let occupied = count_occupied_surrounding(map, x as i32, y as i32);
            if val == b'L' && occupied == 0 {
                b'#'
            } else if val == b'#' && occupied >= 4 {
                b'L'
            } else {
                val
            }
        }
        _ => b'.',
    }
}

fn part2_rule(map: &Map, x: i32, y: i32, val: u8) -> u8 {
    match &val {
        b'#' | b'L' => {
            let occupied = count_occupied_visible(map, x as i32, y as i32);
            if val == b'L' && occupied == 0 {
                b'#'
            } else if val == b'#' && occupied >= 5 {
                b'L'
            } else {
                val
            }
        }
        _ => b'.',
    }
}

fn part1(map: &Map) -> i32 {
    let mut current = map.to_owned();
    // display_map(&current);

    loop {
        let next = step(&current, part1_rule);

        // display_map(&next);

        if current == next {
            return count_occupied(&next);
        }

        current = next;
    }
}

fn part2(map: &Map) -> i32 {
    let mut current = map.to_owned();
    // display_map(&current);

    loop {
        let next = step(&current, part2_rule);

        // display_map(&next);

        if current == next {
            return count_occupied(&next);
        }

        current = next;
    }
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

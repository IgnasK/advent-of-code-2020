use std::{
    collections::{HashMap, HashSet},
    io::BufRead,
};

use lazy_static::lazy_static;
use regex::Regex;

type Ingredient = String;
type Alergen = String;
#[derive(Debug)]
struct Food(HashSet<Ingredient>, HashSet<Alergen>);

lazy_static! {
    static ref LINE_REGEX: Regex = Regex::new(r"^(.+) \(contains (.+)\)$").unwrap();
}

fn read_input() -> Vec<Food> {
    std::io::stdin()
        .lock()
        .lines()
        .map(|l| {
            let l = l.unwrap();

            let matches = LINE_REGEX.captures(&l).unwrap();

            let ingredients: HashSet<Ingredient> = matches
                .get(1)
                .unwrap()
                .as_str()
                .split(" ")
                .map(|v| v.to_owned())
                .collect();
            let alergens: HashSet<Alergen> = matches
                .get(2)
                .unwrap()
                .as_str()
                .split(", ")
                .map(|v| v.to_owned())
                .collect();

            Food(ingredients, alergens)
        })
        .collect()
}

fn resolve_alergens(input: &Vec<Food>) -> HashMap<Ingredient, Alergen> {
    let mut candidates: HashMap<Alergen, HashSet<Ingredient>> = HashMap::new();

    for food in input {
        for alergen in &food.1 {
            if candidates.contains_key(alergen) {
                let set = candidates.get_mut(alergen).unwrap();
                *set = set.intersection(&food.0).map(|v| v.to_owned()).collect();
            } else {
                candidates.insert(alergen.to_owned(), food.0.to_owned());
            }
        }
    }

    // println!("{:?}", &candidates);

    let mut assignments: HashMap<Ingredient, Alergen> = HashMap::new();

    loop {
        let assignable: Vec<(Alergen, Ingredient)> = candidates
            .iter()
            .filter(|(_, v)| v.len() == 1)
            .map(|(k, v)| (k.to_owned(), v.iter().next().unwrap().to_owned()))
            .collect();

        if assignable.len() == 0 {
            break;
        }

        for (a, i) in assignable {
            for (_, ingredients) in candidates.iter_mut() {
                ingredients.remove(&i);
            }
            assignments.insert(i, a);
        }
    }

    assignments
}

fn part1(input: &Vec<Food>, assignments: &HashMap<Ingredient, Alergen>) -> usize {
    input
        .iter()
        .map(|f| f.0.iter().filter(|&i| !assignments.contains_key(i)).count())
        .sum()
}

fn part2(assignments: &HashMap<Ingredient, Alergen>) -> String {
    let mut dangerous: Vec<(&Ingredient, &Alergen)> = assignments.iter().collect();
    dangerous.sort_by_key(|(_, v)| v.to_owned());
    let dangerous_ingredients: Vec<Ingredient> = dangerous.into_iter().map(|(k, _)| k.to_owned()).collect();
    dangerous_ingredients.join(",")
}

fn main() {
    let input = read_input();
    // println!("{:?}", &input);

    let assignments = resolve_alergens(&input);

    println!("part1: {}", part1(&input, &assignments));
    println!("part2: {}", part2(&assignments));
}

use std::collections::{HashMap, VecDeque};
use std::io::Read;

fn read_input() -> VecDeque<u32> {
    let mut buffer = String::new();
    std::io::stdin().read_to_string(&mut buffer).unwrap();

    buffer
        .trim()
        .chars()
        .map(|c| c.to_digit(10).unwrap())
        .collect()
}

fn part1(input: &VecDeque<u32>) -> u32 {
    let mut numbers = input.clone();

    for _ in 0..100 {
        let front = numbers.pop_front().unwrap();
        let next: [u32; 3] = [
            numbers.pop_front().unwrap(),
            numbers.pop_front().unwrap(),
            numbers.pop_front().unwrap(),
        ];
        let mut needle = front - 1;

        loop {
            match numbers.iter().position(|&v| v == needle) {
                Some(index) => {
                    numbers.rotate_left(index + 1);
                    for v in next.iter().rev() {
                        numbers.push_front(*v);
                    }
                    numbers.rotate_right(index + 1);
                    break;
                }
                None => {
                    if needle == 0 {
                        needle = 9;
                    } else {
                        needle -= 1;
                    }
                }
            }
        }
        numbers.push_back(front);
    }

    numbers.rotate_left(numbers.iter().position(|&v| v == 1).unwrap());
    numbers.iter().skip(1).fold(0, |acc, &v| acc * 10 + v)
}

fn part2(input: &VecDeque<u32>) -> u64 {
    let max = 1_000_000;

    let mut indices: HashMap<u32, dlv_list::Index<u32>> = HashMap::new();
    let mut list = dlv_list::VecList::<u32>::new();
    for i in 0..max {
        let value = if let Some(&v) = input.get(i) {
            v
        } else {
            (i + 1) as u32
        };
        let index = list.push_back(value);
        indices.insert(value, index);
    }

    for i in 0..10_000_000 {
        if i % 100_000 == 0 {
            println!("{}", i);
        }
        let front = list.pop_front().unwrap();
        let next: [u32; 3] = [
            list.pop_front().unwrap(),
            list.pop_front().unwrap(),
            list.pop_front().unwrap(),
        ];
        let mut needle = front - 1;

        loop {
            if needle == 0 {
                needle = max as u32;
            }
            if next.contains(&needle) {
                needle -= 1;
            } else {
                let mut index = indices.get(&needle).unwrap().clone();
                for v in next.iter() {
                    index = list.insert_after(index, *v);
                    indices.insert(*v, index);
                }
                break;
            }
        }
        indices.insert(front, list.push_back(front));
    }

    let mut result = 1;

    let mut iter = indices.get(&1).unwrap().clone();
    for _ in 0..2 {
        match list.get_next_index(iter) {
            Some(i) => {
                result *= *list.get(i).unwrap() as u64;
                iter = i;
            }
            None => {
                iter = list.indices().next().unwrap();
            }
        }
    }

    result
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}

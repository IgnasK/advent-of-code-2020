use std::io::{self, Read};
use std::collections::BTreeSet;

fn read_input() -> Vec<i32> {
    let mut file = String::new();
    io::stdin().read_to_string(&mut file).expect("fail to read input");

    let numbers = file.split_whitespace().map(|x| x.parse::<i32>().expect("not an integer")).collect();

    numbers
}

fn part1(numbers: &BTreeSet<i32>) -> i32 {
    for a in numbers {
        let b = 2020-a;
        if numbers.contains(&b) {
            let product = a * b;
            return product
        }
    }
    return 0
}

fn part2(numbers: &BTreeSet<i32>) -> i32 {
    for a in numbers {
        for b in numbers {
            let c = 2020-a-b;
            if c < 0 {
                break
            }

            if numbers.contains(&c) {
                let product = a * b * c;
                return product
            }
        }
    }
    return 0
}

fn main() {
    let input = read_input();

    let set: BTreeSet<i32> = input.iter().cloned().collect();

    println!("part1: {}", part1(&set));
    println!("part1: {}", part2(&set));
}
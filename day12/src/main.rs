use std::io::{self, BufRead};
use std::ops;
#[derive(Debug)]
enum Action {
    N(i32),
    S(i32),
    W(i32),
    E(i32),
    L(i32),
    R(i32),
    F(i32),
}

#[derive(Debug, Copy, Clone, PartialEq)]
struct Position(i32, i32);

impl ops::Add for Position {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self(self.0 + other.0, self.1 + other.1)
    }
}

impl ops::Sub for Position {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self(self.0 - other.0, self.1 - other.1)
    }
}

impl ops::Mul<i32> for Position {
    // The multiplication of rational numbers is a closed operation.
    type Output = Self;

    fn mul(self, rhs: i32) -> Self {
        Self(self.0 * rhs, self.1 * rhs)
    }
}

impl Position {
    fn manhattan(self, other: Self) -> i32 {
        let diff = self - other;
        diff.0.abs() + diff.1.abs()
    }

    fn rotate_around_origin(self) -> Self {
        Self(self.1, -self.0)
    }
}

// Positive north-east, clockwise, starting east
static DIRECTIONS: [Position; 4] = [
    Position(1, 0),
    Position(0, -1),
    Position(-1, 0),
    Position(0, 1),
];

fn read_input() -> Vec<Action> {
    io::stdin()
        .lock()
        .lines()
        .map(|l| {
            let command = l.unwrap();
            let command_type = &command[0..1];
            let command_number = (&command[1..]).parse::<i32>().unwrap();

            match command_type {
                "N" => Action::N(command_number),
                "S" => Action::S(command_number),
                "W" => Action::W(command_number),
                "E" => Action::E(command_number),
                "L" => Action::L(command_number),
                "R" => Action::R(command_number),
                "F" => Action::F(command_number),
                _ => panic!("unknown command type {}", command_type),
            }
        })
        .collect()
}

fn clockwise_turns(action: &Action) -> i32 {
    match action {
        Action::L(i) => (-i / 90) % 4 + 4,
        Action::R(i) => i / 90,
        _ => 0,
    }
}

fn part1(input: &Vec<Action>) -> i32 {
    let mut position = Position(0, 0);
    let mut direction = 0;

    for action in input {
        match action {
            Action::N(i) => position.1 += i,
            Action::S(i) => position.1 -= i,
            Action::E(i) => position.0 += i,
            Action::W(i) => position.0 -= i,
            Action::L(_) | Action::R(_) => direction = (direction + clockwise_turns(action)) % 4,
            Action::F(i) => position = position + DIRECTIONS[direction as usize] * *i,
        }
    }

    Position::manhattan(position, Position(0, 0))
}

fn part2(input: &Vec<Action>) -> i32 {
    let mut position = Position(0, 0);
    let mut waypoint = Position(10, 1);

    for action in input {
        match action {
            Action::N(i) => waypoint.1 += i,
            Action::S(i) => waypoint.1 -= i,
            Action::E(i) => waypoint.0 += i,
            Action::W(i) => waypoint.0 -= i,
            Action::L(_) | Action::R(_) => {
                for _ in 0..(clockwise_turns(action) % 4) {
                    waypoint = waypoint.rotate_around_origin();
                }
            }
            Action::F(i) => position = position + waypoint * *i,
        }
    }

    Position::manhattan(position, Position(0, 0))
}

fn main() {
    let input = read_input();

    println!("part1: {}", part1(&input));
    println!("part2: {}", part2(&input));
}
